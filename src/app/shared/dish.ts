import { Comment } from './comment'; //new folder comment created

export class Dish {  //would help to construct the menu for restaurant
    id: string;
    name: string; 
    image: string;
    category: string;
    featured?: boolean;
    label: string;
    price: string;
    description: string;
    comments?: Comment[]; //new property called comments

    constructor(){
        this.id = '';
        this.name = '';
        this.image = '';
        this.category = '';
        this.label = '';
        this.price = '';
        this.description = '';
    }
}
import { Component, OnInit } from '@angular/core';
import { Dish } from '../shared/dish';
import { DISHES } from '../shared/dishes';


/*
const DISHES: Dish[] = [  //variable dishes which will be of an array of Dish[] type (imported) = include the set of dishes into the 'dishes' variable.
{
  id: '0',
  name: 'Uthappizza',
  image: '/assets/images/uthappizza.png',
  category: 'mains',
  featured: true,
  label: 'Hot',
  price: '4.99',
  // tslint:disable-next-line:max-line-length
  description: 'A unique combination of Indian Uthappam (pancake) and Italian pizza, topped with Cerignola olives, ripe vine cherry tomatoes, Vidalia onion, Guntur chillies and Buffalo Paneer.'
},
{
  id: '1',
  name: 'Zucchipakoda',
  image: '/assets/images/zucchipakoda.png',
  category: 'appetizer',
  featured: false,
  label: '',
  price: '1.99',
  description: 'Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce'
},
{
  id: '2',
  name: 'Vadonut',
  image: '/assets/images/vadonut.png',
  category: 'appetizer',
  featured: false,
  label: 'New',
  price: '1.99',
  description: 'A quintessential ConFusion experience, is it a vada or is it a donut?'
},
{
  id: '3',
  name: 'ElaiCheese Cake',
  image: '/assets/images/elaicheesecake.png',
  category: 'dessert',
  featured: false,
  label: '',
  price: '2.99',
  description: 'A delectable, semi-sweet New York Style Cheese Cake, with Graham cracker crust and spiced with Indian cardamoms'
}
];    // For the Dish[] each one of them is a JS object here, so that it can be mapped into the corresponding typescript class there. Each JS object has properties that exactly match the properties defined in Dish class (dish.ts). Now this becomes available to use when the html template is defined for menu.
*/

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  dishes: Dish[] = DISHES; //you need not inform the object about it's type since DISHES already knows it's type typescript is smart enough to know what type the value is of (i.e. dishes: Dish[] = DISHES).
  /*the selectedDish will be assigned the vaue will be based upon which dish I select*/
  selectedDish: Dish = DISHES[0];

  constructor() { }

  ngOnInit() {
  }

  onSelect(dish: Dish) {
    this.selectedDish = dish;
  }

}
